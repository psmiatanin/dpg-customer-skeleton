﻿using DPG.Customer.Application.Contract.Interfaces;
using DPG.Customer.Application.Contract.Models;
using DPG.Customer.Shared;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace DPG.Customer.WebApi.Controllers
{
    [Route("api/customers/{id}/personal-details")]
    [ApiController]
    public class PersonalDetailsController : ControllerBase
    {
        private readonly ICustomerPersonalDetailsService _service;

        public PersonalDetailsController(ICustomerPersonalDetailsService service)
        {
            this._service = service.EnsureIsNotNull(nameof(service));
        }

        [HttpGet]
        public Task<PersonalDetails> Get(string id)
        {
            return this._service.GetAsync(id);
        }
    }
}
