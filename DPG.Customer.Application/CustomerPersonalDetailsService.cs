﻿using DPG.Customer.Application.Contract.Dependencies;
using DPG.Customer.Application.Contract.Interfaces;
using DPG.Customer.Application.Contract.Models;
using DPG.Customer.Shared;
using System;
using System.Threading.Tasks;

namespace DPG.Customer.Application
{
    public class CustomerPersonalDetailsService : ICustomerPersonalDetailsService
    {
        private readonly ICustomrPersonalDetailsRepository _repository;

        public CustomerPersonalDetailsService(ICustomrPersonalDetailsRepository repository)
        {
            _repository = repository.EnsureIsNotNull(nameof(repository));
        }

        public Task<PersonalDetails> GetAsync(string customerId)
        {
            customerId.EnsureIsNotNullOrWhitespace(nameof(customerId));

            return this._repository.GetAsync(customerId);
        }

        public Task UpdateAsync(PersonalDetails details)
        {
            details.EnsureIsNotNull(nameof(details));

            //TODO: Application validation logic is here
            // Fluent validation or data annotation validation
            // data validators as strategy

            throw new NotImplementedException();
        }
    }
}
