﻿using DPG.Customer.Application.Contract.Models;
using System;
using System.Threading.Tasks;

namespace DPG.Customer.Application.Contract.Dependencies
{
    public interface ICustomrPersonalDetailsRepository
    {
        Task<PersonalDetails> GetAsync(string customerId);

        Task UpdateAsync(PersonalDetails details);
    }
}
