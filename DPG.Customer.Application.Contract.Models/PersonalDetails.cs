﻿using System;

namespace DPG.Customer.Application.Contract.Models
{
    public class PersonalDetails
    {
        public string CustomerId { get; set; }

        public string Name { get; set; }

        public string Surename { get; set; }
    }
}
