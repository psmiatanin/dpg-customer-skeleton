﻿using System;
using System.Threading.Tasks;
using DPG.Customer.Application.Contract.Models;

namespace DPG.Customer.Application.Contract.Interfaces
{
    public interface ICustomerPersonalDetailsService
    {
        Task<PersonalDetails> GetAsync(string customerId);

        Task UpdateAsync(PersonalDetails details);

    }
}
