﻿using DPG.Customer.Application.Contract.Dependencies;
using DPG.Customer.Application.Contract.Models;
using System;
using System.Threading.Tasks;

namespace DPG.Customer.Infrastructure.Azure
{
    public class CustomrPersonalDetailsRepository : ICustomrPersonalDetailsRepository
    {
        public Task<PersonalDetails> GetAsync(string customerId)
        {
            var output = new PersonalDetails()
            {
                CustomerId = customerId,
                Name = "John",
                Surename = "Doe"
            };

            return Task.FromResult(output);
        }

        public Task UpdateAsync(PersonalDetails details)
        {
            throw new NotImplementedException();
        }
    }
}
