﻿using DPG.Customer.Shared;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DPG.Customer.DI
{
    public abstract class RegistrationModule : IRegistrationModule
    {
        public void Configure(IConfiguration configuration, IServiceCollection services)
        {
            configuration.EnsureIsNotNull(nameof(configuration));
            services.EnsureIsNotNull(nameof(services));

            RegisterCore(configuration, services);
        }

        protected abstract void RegisterCore(IConfiguration configuration, IServiceCollection services);
    }
}
