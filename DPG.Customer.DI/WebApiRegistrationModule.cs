﻿using DPG.Customer.Application;
using DPG.Customer.Application.Contract.Dependencies;
using DPG.Customer.Application.Contract.Interfaces;
using DPG.Customer.Infrastructure.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DPG.Customer.DI
{
    public class WebApiRegistrationModule : RegistrationModule
    {
        protected override void RegisterCore(IConfiguration configuration, IServiceCollection services)
        {
            services.AddTransient<ICustomrPersonalDetailsRepository, CustomrPersonalDetailsRepository>();
            services.AddScoped<ICustomerPersonalDetailsService, CustomerPersonalDetailsService>();        
        }
    }
}
