﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DPG.Customer.DI
{
    public interface IRegistrationModule
    {
        void Configure(IConfiguration configuration, IServiceCollection services);
    }
}
