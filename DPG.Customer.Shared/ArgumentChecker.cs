﻿using System;

namespace DPG.Customer.Shared
{
    public static class ArgumentChecker
    {
        private const string VALUE_MUST_NOT_BE_NULL_MESSAGE = "Value must not be null";
        private const string VALUE_MUST_NOT_BE_NULL_OR_WHITESPACE_MESSAGE = "Value must not be null or white space string";

        public static T EnsureIsNotNull<T>(this T item, string argumentName, string error = VALUE_MUST_NOT_BE_NULL_MESSAGE)
        {
            if (item == null)
            {
                throw new ArgumentNullException(argumentName, string.IsNullOrWhiteSpace(error) ? VALUE_MUST_NOT_BE_NULL_MESSAGE : error);
            }

            return item;
        }

        public static string EnsureIsNotNullOrWhitespace(this string item, string argumentName, string error = VALUE_MUST_NOT_BE_NULL_OR_WHITESPACE_MESSAGE)
        {
            if (item == null)
            {
                error = string.IsNullOrWhiteSpace(error) ? VALUE_MUST_NOT_BE_NULL_OR_WHITESPACE_MESSAGE : error;

                throw new ArgumentException(argumentName, error);
            }

            return item;
        }

    }
}
